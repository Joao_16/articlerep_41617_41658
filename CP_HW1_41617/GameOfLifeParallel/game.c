#include "game.h"
#include <stdio.h>
#include <stdlib.h>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>


void game_free(Game *game){

    free(game);
}


int game_cell_is_alive(Game *game, size_t row, size_t col){

    if(game->board[game->cols*row + col] == '1')
        return 1;
    else
        return 0;
}

int game_cell_is_dead(Game *game, size_t row, size_t col){

    if(game->board[game->cols*row + col] == '0')
        return 1;
    else
        return 0;
}


Game *game_new(void){

    Game* game = (Game*)malloc(sizeof(Game));
    game->board = malloc(sizeof(char*));
    return game;
}


int game_parse_board(Game *game, GameConfig *config){

    if(config->input_file == NULL)
        return 1;

    char string[256];
    fgets(string, 256, config->input_file);
    int rows = atoi(&string[5]);
    fgets(string, 256, config->input_file);
    int cols = atoi(&string[5]);

    int size = cols*rows;
    char *array;
    array = malloc(sizeof(char)*size);
    int currentPos = 0;
    char c;

    do{
        c = fgetc(config->input_file);
        if(c=='.'){
            array[currentPos] = '0';
            currentPos++;
       }else if (c == '#'){
            array[currentPos] = '1';
            currentPos++;
        }
    }while(c!=EOF);

    game->cols = cols;
    game->rows = rows;
    game->board = array;
    fclose(config->input_file);
    return 0;
}


void game_print_board(Game *game){

    int i, j;
    for(i = 0; i < game->rows;i++){
        for(j = 0; j < game->cols; j++){
		if( game->board[game->cols*i + j]=='1')
			printf("#");
		else
			printf(".");
        }
        printf("\n");
    }
}


void game_cell_set_alive(Game *game, size_t row, size_t col){
    game->board[game->cols*row + col] = '1';
}


void game_cell_set_dead(Game *game, size_t row, size_t col){
    game->board[game->cols*row + col] = '0';
}


int game_tick(Game *game){

    int i,j;
    char aux[game->cols*game->rows];



    cilk_for(j = 0; j < game->rows; j++){
       cilk_for(i = 0; i < game->cols;i++){
            int counter = processNeighbors(game,i,j);

            if(game_cell_is_alive(game,j,i)){
                 if(counter < 2 || counter > 3)
                    aux[game->cols*j + i] = '0';
                else
                    aux[game->cols*j + i] = '1';

            }else{
                if(counter == 3)
                    aux[game->cols*j + i] = '1';
                else
                      aux[game->cols*j + i] = '0';

            }
            counter = 0;
        }
    }
    for(i = 0; i < game->cols*game->rows;i++){
        game->board[i] = aux[i];
    }

    return 0;
}

int processNeighbors(Game *game, int i, int j){
    int counter = 0;

            int x, y;
            cilk_for(x = -1; x <= 1; x++){
                cilk_for(y = -1 ; y <= 1 ;y++){
                    int posX = x + i;
                    int posY = y + j;
                    if(posX < 0)
                        posX = game->cols-1;
                    if(posY < 0)
                        posY = game->rows-1;

                    if(posX >= game->cols)
                        posX = 0;
                    if(posY >= game->rows)
                        posY = 0;

                    if(game_cell_is_alive(game,posY,posX))
                            counter++;

                }
            }
             if(game_cell_is_alive(game,j,i))
                        counter--;
    return counter;
}


int main(int argc, char*argv[]){


   GameConfig * config = game_config_new_from_cli(argc,argv);

    Game *game =  game_new();
    game_parse_board(game,config);

    game_print_board(game);printf("\n");

    int i;
    for(i = 1; i <= config->generations;i++){
        game_tick(game);
        if(config->debug){
           printf("Generation %d :\n",i); game_print_board(game);printf("\n");
        }
    }
    game_print_board(game);
    return 0;
}
