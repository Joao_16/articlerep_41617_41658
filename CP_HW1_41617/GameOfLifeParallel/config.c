#include "config.h"
#include <unistd.h>
#include <stdio.h>
#include <string.h>


void game_config_free(GameConfig* config){
    free(config);
}


size_t game_config_get_generations(GameConfig* config){

    return config->generations;
}

int game_config_get_debug(GameConfig *config){

    return config->debug;
}
int game_config_get_silent(GameConfig *config){

    return config->silent;
}


GameConfig *game_config_new_from_cli(int argc, char *argv[]){
 
    GameConfig* config = (GameConfig*)malloc(sizeof(GameConfig));

    config->generations = 20;
    config->debug = 0;
    config->silent = 0;

    char *filename= malloc(sizeof(char));
    strcpy(filename,"tests/");
    strcat(filename,argv[argc-1]);

    FILE * file;
    file = fopen(filename,"r");


    config->input_file = file;

    int c;
    while ((c = getopt(argc, argv, "dn:s")) != -1) {
        switch(c){

        case 'd':
            config->debug = 1;
            break;

        case 'n':
            config->generations = atoi(optarg);
            break;

        case 's':
             config->silent = 1;
            break;

        }

    }

    return config;
}
